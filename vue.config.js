module.exports = {
  devServer: {
    port: 6789
  },
  transpileDependencies: [
    'vuetify'
  ],
  productionSourceMap: false
};
