import '@mdi/font/css/materialdesignicons.css'
import 'vuetify/lib/styles/main.sass'
import { createVuetify } from 'vuetify'
import * as components from 'vuetify/lib/components'
// import { VCard, VCardText, VCardTitle, VApp, VMain, VBtn } from 'vuetify/lib/components'
import * as directives from 'vuetify/lib/directives'

export default createVuetify({
  components,
  // components: {
  //   ...components
  // },
  directives,
  theme: {
    defaultTheme: 'dark',
    themes: {
      light: {
        colors: {
          primary: '#1867c0',
          success: '#4caf50',
          warning: '#fb8c00',
          info: '#2196f3',
          secondary: '#5cbbf6',
          accent: '#005caf',
          error: '#ff5252',
          "on-primary": '#ffffff',
          "on-success": '#ffffff',
          "on-warning": '#ffffff',
          "on-info": '#ffffff',
          "on-secondary": '#ffffff',
          "on-accent": '#ffffff',
          "on-error": '#ffffff'
        }
      },
      dark: {
        colors: {
          primary: '#1867c0',
          success: '#4caf50',
          warning: '#fb8c00',
          info: '#2196f3',
          secondary: '#5cbbf6',
          accent: '#005caf',
          error: '#ff5252',
          "on-primary": '#ffffff',
          "on-success": '#ffffff',
          "on-warning": '#ffffff',
          "on-info": '#ffffff',
          "on-secondary": '#ffffff',
          "on-accent": '#ffffff',
          "on-error": '#ffffff'
        }
      }
    }
  }
})
