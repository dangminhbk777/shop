import Http from '../utils/http'
import Path from './../constants/path-api'

export default {
  profile(id) {
    console.log('profile --' + id);
    return Http.GET(`${Path.USER.PROFILE}/${id}`);
  }
}

