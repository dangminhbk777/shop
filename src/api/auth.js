import Http from '../utils/http'
import Path from './../constants/path-api'

export default {
  login (request) {
    return Http.POST(Path.AUTH.LOGIN, request);
  },
  logout (request) {
    return Http.GET(Path.AUTH.LOGOUT, request);
  }
}

