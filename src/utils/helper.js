const configHeaderApi = () => {
  return {
    'Cache-Control': 'no-cache, no-store, must-revalidate',
    'Pragma': 'no-cache',
    'Expires': '0',
    // 'Accept-Language': Store.getters.getLang,
    // 'Authorization': window.$cookies.isKey('Authorization') ? window.$cookies.get('Authorization') : '',
    'X-SentTime': '123456'
  }
};

export default {
  configHeaderApi
}
