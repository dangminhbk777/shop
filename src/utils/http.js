import axios from 'axios'
import Properties from '../constants/properties'
import Helper from './../utils/helper'
import { useRouter } from 'vue-router'

const http = axios.create({
  baseURL: Properties.SERVER_LINK
});
http.defaults.timeout = 300000;
const router = useRouter();

const GET = (url, params, showToast = false, loading = false) => {
  console.log(showToast);
  console.log(loading);
  return new Promise(resolve => {
    http.get(
      url,
      {
        headers: Helper.configHeaderApi(),
        params: {...params}
      })
      .then(baseResponse => {
        resolve(baseResponse)
      })
      .catch(error => {
        handlerRejected(error);
      })
  })
};

const POST = (url, body, showToast = false, loading = false) => {
  console.log(showToast);
  console.log(loading);
  return new Promise(resolve => {
    http.post(
      url,
      body,
      {
        headers: Helper.configHeaderApi(),
      })
      .then(baseResponse => {
        resolve(baseResponse)
      })
      .catch(error => {
        handlerRejected(error);
      })
  })
};

const handlerRejected = (error) => {
  console.error(error);
  if (error.message.includes('401')) {
    router.push('/login').catch(() => {})
  }
};

export default { GET, POST }
