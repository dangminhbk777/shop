const PATH = {
  // Authentication
  AUTH: {
    LOGIN: '/auth/login',
    LOGOUT: '/auth/logout'
  },
  // User
  USER: {
    PROFILE: '/user/profile'
  }
};
export default PATH
