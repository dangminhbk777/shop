const Properties = {
  SERVER_LINK: 'http://localhost:1111',
  CONTEXT_PATH: '',
  WEBSOCKET: 'http://localhost:8888/drone/ws'
};

if (typeof document['CONFIG_WEB_APP'] !== 'undefined') {
  Properties.SERVER_LINK = document['CONFIG_WEB_APP']['PROPERTIES'].SERVER_LINK()
    .concat(document['CONFIG_WEB_APP']['PROPERTIES'].CONTEXT_PATH());
  Properties.CONTEXT_PATH = document['CONFIG_WEB_APP']['PROPERTIES'].CONTEXT_PATH();
  Properties.WEBSOCKET = document['CONFIG_WEB_APP']['PROPERTIES'].WEBSOCKET();
}

export default Properties
