const CONSTANT = {
  HTTP_STATUS: {
    SUCCESS: 'ES200',
    WARNING: 'ES201'
  },
  SELECTED: {
    TRUE: 2,
    FALSE: 1
  },
  SCREEN: {
    USER: 'user',
    PROJECT: 'project',
    DEVICE: 'device',
    AREA: 'area',
    MEDIA: 'media',
    COMPANY: 'company',
    ANALYSIS: 'analysis',
    ACTIVITY: 'activity'
  },
  EXPORT: {
    CSV: 'csv',
    PDF: 'pdf'
  }
};

export default CONSTANT
