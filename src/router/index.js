import { createWebHashHistory, createRouter } from "vue-router";

const Home = () => import(/* webpackChunkName: "home" */ './../views/Home.vue');
const Profile = () => import(/* webpackChunkName: "profile" */ './../views/Profile.vue');
const NotFound = () => import(/* webpackChunkName: "not-found" */ './../views/NotFound.vue');
const Login = () => import(/* webpackChunkName: "login" */ './../views/Login.vue');

const routes = [
  {
    path: "/",
    name: "Home",
    component: Home,
  },
  {
    path: "/profile",
    name: "Profile",
    component: Profile,
  },
  {
    path: "/login",
    name: "Login",
    component: Login,
  },
  {
    path: "/:catchAll(.*)",
    component: NotFound,
  }
];

const router = createRouter({
  history: createWebHashHistory(),
  routes
});

export default router;
